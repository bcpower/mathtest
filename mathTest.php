<?php
require_once('layout.php');
require_once('userAuth.php');

print getHTMLNewPage();
print getHeader();
print getMenu();

print <<<HTML
<div id="page">
  <br/>
  <div id="mathTest">
    <div id="problem">
      <div id="current">
        <div id='question'>
        </div>
          <br/>
          <br/>
        <div id="answers">
          <input type="text" name="answer" id="answer"/>
          <input type="button" value="Answer" onclick="updateQuestion()"/>
        </div>
      </div>
      <div id="pastP">
        Past Questions
        <table id='past'>
        </table>
      </div>
    </div>
  </div>
</div>
HTML;

print getFooter();

?>
