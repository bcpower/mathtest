<?php
require_once('../mysql_setup.php');
require_once('../userAuth.php');

function getProblem($pid,$dbh){
  $problem_stmt = $dbh->prepare("Select problem from problems where pid = ? limit 1");
  $problem_stmt->bind_param('i',$pid);
  $problem_stmt->execute(); 
  $problem_stmt->bind_result($result);
  $problem_stmt->fetch();
  $problem_stmt->close();

  return $result;
}

function evalExpression($prob){
  $f = create_function('','return (int)(' . $prob . ');');  
  return $f();
}
function storeAnswer($pid,$uid,$status,$dbh){
  $answered_stmt = $dbh->prepare("insert into answered_problems values('',?,?,?)");
  $answered_stmt->bind_param("iis",$pid,$uid,$status);
  $answered_stmt->execute(); 
  $answered_stmt->close();
}
$pid = $_POST['pid'];
$uid = getUserId();
$problem = getProblem($pid,$dbh);
$answer = $_POST['answer'];
$result = evalExpression($problem);

if($result == $answer){
  $status = "Correct";
}else{
  $status = "Wrong";
}

if($uid >0){
  storeAnswer($pid,$uid,$status,$dbh);
}
header('Content-type: application/json');
print json_encode(array("result"=>$status));

?>
