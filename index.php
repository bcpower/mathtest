<?php
require_once('layout.php');
require_once('userAuth.php');

print getHTMLNewPage();
print getHeader();
print getMenu();

print <<<HTML
    <div id="page">
    <div id="pageTitle">Welcome to Math Test</div>
HTML;
if(isLoggedIn()){
  $user_name = getUserName();
  print '<div id="artical"> Thanks for coming back '.$user_name.'</div>';
}else{
  print '<div id="artical">     This is just a silly math game. Feel free to play now or register.<br/>
      By registering and playing while logged in you will be able to see statistics about your games.</div>
      </div>';
}

print getFooter();

?>
