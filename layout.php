<?php

function getHTMLNewPage(){
  $html = <<<HTML
<html>
  <title>Math Test</title>
  <head>
    <script language="javascript" src="./javascript/jquery.min.js"></script>
    <script language="javascript" src="./javascript/mathTest.js"></script>
    <link rel="stylesheet" type="text/css" href="./css/mathTest.css"/>
  </head>
  <body>
HTML;

  return $html;
}
function getHeader(){
  $header = <<<HTML
  <div id="header">
    <div id="title">Math Test</div>
    <div id="subTitle">A silly game by Berton</div>
  </div>
HTML;
  
  return $header;
}
function getMenu(){
  $menu = <<<HTML
  <div id="menu">
    <ul>
      <li><a href="index.php">Home</a></li>
      <li><a href="mathTest.php">Math Test</a></li>
HTML;

  if(isLoggedIn()){
    $menu .= ' <li><a href="statistics.php">Statistics</a></li>';
    $menu .= ' <li><a href="logout.php">Log Out</a></li>';
  }else{
    $menu .= ' <li><a href="login.php">Log In</a></li>';
    $menu .= ' <li><a href="register.php">Register</a></li>';
  }
  $menu .= <<<HTML
    </ul>
  </div>
HTML;

  return $menu;
}
function getFooter(){
  $footer = <<<HTML
  </body>
</html>
HTML;

  return $footer;
}
