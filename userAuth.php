<?php
require_once('mysql_setup.php');
session_start();

  function isLoggedIn(){
    if(isset($_SESSION['uid'])){
      return true; 
    }
    return false;  
  }
  function getUserName(){
    if(isLoggedIn()) {
      $first_name = $_SESSION['first_name'];
      $last_name = $_SESSION['last_name'];
      $user_name =  $first_name. " " .$last_name;

      return $user_name;
    }
    return false;
  }
  function getUserId(){
    if(isLoggedIn()) {
      return $_SESSION['uid'];
    }
    return false;
  }
  function getUserEmail(){
    if(isLoggedIn()) {
      return $_SESSION['email'];
    }
    return false;
  }
  function registerUser($first_name,$last_name,$email,$password,$dbh){
    $uid = checkForExistingUser($email,$dbh);
    if($uid > 0){
      return "User Already exists";
    }
    $password_hash = sha1($password);
    $create_stmt = $dbh->prepare("insert into user values('',?,?,?,?)");
    $create_stmt->bind_param('ssss',$email,$first_name,$last_name,$password_hash);
    $create_stmt->execute();
    $affected_rows = $create_stmt->affected_rows;
    $create_stmt->close();

    if($affected_rows == 1){
      loginUser($email,$password,$dbh);
      return "You are now Registered. Thanks";
    }

    return "An error Occured"; 
  }
  function checkForExistingUser($email,$dbh){
    $existing_stmt = $dbh->prepare("Select uid from user where email = ? limit 1");
    $existing_stmt->bind_param('s',$email);
    $existing_stmt->execute();
    $existing_stmt->bind_result($result);
    $existing_stmt->fetch();
    $existing_stmt->close();

    return $result;
  }
  function loginUser($email,$password,$dbh){
    $existing = checkForExistingUser($email,$dbh);
    if($existing > 1){
      $login_stmt = $dbh->prepare("Select uid,first_name,last_name from user 
                                      where email = ? and password = ? limit 1");
      $login_stmt->bind_param('ss',$email,sha1($password));
      $login_stmt->execute();
      $login_stmt->bind_result($uid,$first_name,$last_name);
      $login_stmt->fetch();

      if(isset($uid) and $uid > 0){
        $_SESSION['first_name'] = $first_name;
        $_SESSION['last_name'] = $last_name;
        $_SESSION['uid'] = $uid;
        $_SESSION['email'] = $email;
      }else{
        return "Login Error";
      }
      $login_stmt->close();
    }else{
      return "Login Error2";
    }
  }
  function logout(){
    unset($_SESSION['first_name']);
    unset($_SESSION['last_name']);
    unset($_SESSION['uid']);
    unset($_SESSION['email']);
  }
?>
