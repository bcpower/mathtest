<?php
require_once('../mysql_setup.php');

function getRandomProblem($min,$max,$dbh){
  $row = rand($min,$max);
  $rand_problem_sql = "Select pid,problem from problems where pid = $row limit 1";
  $result = $dbh->query($rand_problem_sql);
  $rand_problem = $result->fetch_assoc();
  $result->close();
   
  return $rand_problem;
}

function getMaxMinRows($dbh){
  $max_row_sql = "select max(pid) as 'MAX', min(pid) as 'MIN' from problems limit 1";
  $result = $dbh->query($max_row_sql);
  $max_row = $result->fetch_assoc();
  $result->close();

  $max_min = array('MAX'=>$max_row['MAX'],'MIN'=>$max_row['MIN']);
  return $max_min;
}

$max_min = getMaxMinRows($dbh);
$problem = getRandomProblem($max_min['MIN'],$max_min['MAX'],$dbh);

header('Content-type: application/json');
print json_encode($problem);
