<?php
require_once('layout.php');
require_once('userAuth.php');

print getHTMLNewPage();
print getHeader();
print getMenu();

if(isset($_POST['email'])){
  $email = $_POST['email'];
  $password = $_POST['password'];
  $result = loginUser($email,$password,$dbh);
  if(isLoggedIn()){
    print '<div id="page">
              <br/>';
    print "You have been successfully been logged in";

    print " </div>";
    print getFooter();
    exit;
  }
}

print <<<HTML
  <div id="page">
    <br/>
    <div id="login">
      <form id="loginForm" action="login.php" method="post">
      <label for="email">Email:</label> <input style="margin-left:20pt;" type="text" name="email"/><br/>
      <label for="password">Password:</label> <input type="password" name="password"/><br/>
      <input type="submit" value="Login"/>
      </form>
    </div>

  </div>
HTML;

print getFooter();

?>
