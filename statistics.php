<?php
require_once('layout.php');
require_once('userAuth.php');

print getHTMLNewPage();
print getHeader();
print getMenu();

$uid = getUserId();
$correct = getCorrect($uid,$dbh);
$wrong = getWrong($uid,$dbh);
$total = $wrong + $correct;

print '<div id="page">
    <div id="pageTitle"> Statistics </div>
    <div id="artical">You have answered '.$total.' questions.<br/>
      '.$correct.' were correct and '.$wrong.' were wrong.
    </div>
  </div>';

print getFooter();


function getWrong($uid,$dbh){
  $problem_stmt = $dbh->prepare("Select count(*) as 'Wrong' 
        from answered_problems where uid = ? and answered = 'Wrong' ");
  $problem_stmt->bind_param('i',$uid);
  $problem_stmt->execute();
  $problem_stmt->bind_result($result);
  $problem_stmt->fetch();
  $problem_stmt->close();

  return $result;
}
function getCorrect($uid,$dbh){
  $problem_stmt = $dbh->prepare("Select count(*) as 'Correct' 
        from answered_problems where uid = ? and answered = 'Correct' ");
  $problem_stmt->bind_param('i',$uid);
  $problem_stmt->execute();
  $problem_stmt->bind_result($result);
  $problem_stmt->fetch();
  $problem_stmt->close();

  return $result;
}
?>
